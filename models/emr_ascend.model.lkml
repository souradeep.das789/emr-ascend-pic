connection: "preprod-hive"

# include all the views
include: "/views/**/*.view"

datagroup: emr_ascend_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "2 hour"
}

persist_with: emr_ascend_default_datagroup

explore: emr_ascend {}
explore: ascend {}

explore: t_ml_order_details {
  join: t_ml_cust_demographics {
    type: inner
    sql_on: ${t_ml_cust_demographics.acct_id} = ${t_ml_order_details.acct_id} and ${t_ml_cust_demographics.dsn} = ${t_ml_order_details.dsn} ;;
    relationship: many_to_one
  }

  join: t_event {
    type: inner
    sql_on: ${t_event.event_id} = ${t_ml_order_details.event_id} and ${t_event.dsn} = ${t_ml_order_details.dsn} ;;
    relationship: many_to_many
  }

  join: t_ticket {
    type: inner
    sql_on: ${t_ticket.event_id} = ${t_event.event_id}
      and ${t_ticket.dsn} = ${t_event.dsn};;
    relationship: many_to_one

  }
  join: t_season {
    type: inner
    sql_on: ${t_season.season_id} = ${t_event.season_id} and ${t_season.dsn} = ${t_event.dsn} ;;
    relationship: one_to_many

  }

  join: t_arena {
    type: inner
    sql_on: ${t_season.arena_id} = ${t_arena.arena_id} and ${t_season.dsn} = ${t_arena.dsn} ;;
    relationship: many_to_one
  }

  join: t_ticket_barcode {
    type: left_outer
    sql_on: ${t_ml_order_details.event_id} = ${t_ticket_barcode.event_id}
      and ${t_ml_order_details.section_id} = ${t_ticket_barcode.section_id}
      and ${t_ml_order_details.row_id} = ${t_ticket_barcode.row_id}
      and ${t_ticket_barcode.dsn} = ${t_ml_order_details.dsn}
     --and MOD(t_ticket_barcode."SEAT_NUM", t_ml_order_details."SEAT_INCREMENT") = MOD(t_ml_order_details."SEAT_NUM", t_ml_order_details."SEAT_INCREMENT")
      --and ${t_ticket_barcode.status} = 'Y'
      --and ${t_ticket_barcode.acct_id} = ${t_ml_order_details.acct_id};;
    relationship: many_to_one
  }
}
