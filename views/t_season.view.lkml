view: t_season {
  sql_table_name: safetix.t_season ;;
  suggestions: no

  dimension: active {
    type: string
    sql: ${TABLE}.active ;;
  }

  dimension: ada_query_plan_id {
    type: string
    sql: ${TABLE}.ada_query_plan_id ;;
  }

  dimension: add_date {
    type: string
    sql: ${TABLE}.add_date ;;
  }

  dimension: archtics_ism_map_id {
    type: string
    sql: ${TABLE}.archtics_ism_map_id ;;
  }

  dimension: archtics_ism_map_type {
    type: string
    sql: ${TABLE}.archtics_ism_map_type ;;
  }

  dimension: arena_id {
    type: string
    sql: ${TABLE}.arena_id ;;
  }

  dimension: auto_expand_plans_days {
    type: string
    sql: ${TABLE}.auto_expand_plans_days ;;
  }

  dimension: barcode_season_key {
    type: string
    sql: ${TABLE}.barcode_season_key ;;
  }

  dimension: barcode_seatcode_adjustment {
    type: string
    sql: ${TABLE}.barcode_seatcode_adjustment ;;
  }

  dimension: bootstrap {
    type: yesno
    sql: ${TABLE}.bootstrap ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}.dsn ;;
  }

  dimension: host_system {
    type: string
    sql: ${TABLE}.host_system ;;
  }

  dimension: host_tcode {
    type: string
    sql: ${TABLE}.host_tcode ;;
  }

  dimension: line1 {
    type: string
    sql: ${TABLE}.line1 ;;
  }

  dimension: line2 {
    type: string
    sql: ${TABLE}.line2 ;;
  }

  dimension: line3 {
    type: string
    sql: ${TABLE}.line3 ;;
  }

  dimension: manifest_id {
    type: string
    sql: ${TABLE}.manifest_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: season_id {
    type: string
    sql: ${TABLE}.season_id ;;
  }

  dimension: season_year {
    type: string
    sql: ${TABLE}.season_year ;;
  }

  dimension: seat_query_plan_id {
    type: string
    sql: ${TABLE}.seat_query_plan_id ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: upd_datetime {
    type: string
    sql: ${TABLE}.upd_datetime ;;
  }

  dimension: upd_user {
    type: string
    sql: ${TABLE}.upd_user ;;
  }

  measure: count {
    type: count
    drill_fields: [name]
  }
}
