view: t_event {
  sql_table_name: safetix.t_event ;;
  drill_fields: [emt_event_id]
  suggestions: no

  dimension: emt_event_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.emt_event_id ;;
  }

  dimension: ada_query_plan_id {
    type: string
    sql: ${TABLE}.ada_query_plan_id ;;
  }

  dimension: add_date {
    type: string
    sql: ${TABLE}.add_date ;;
  }

  dimension: am_id {
    type: string
    sql: ${TABLE}.am_id ;;
  }

  dimension: auto_add_membership_id1 {
    type: string
    sql: ${TABLE}.auto_add_membership_id1 ;;
  }

  dimension: auto_add_membership_id2 {
    type: string
    sql: ${TABLE}.auto_add_membership_id2 ;;
  }

  dimension: auto_add_membership_id3 {
    type: string
    sql: ${TABLE}.auto_add_membership_id3 ;;
  }

  dimension: auto_add_membership_id_fwd {
    type: string
    sql: ${TABLE}.auto_add_membership_id_fwd ;;
  }

  dimension: auto_add_membership_id_resale {
    type: string
    sql: ${TABLE}.auto_add_membership_id_resale ;;
  }

  dimension: barcode_event_id {
    type: string
    sql: ${TABLE}.barcode_event_id ;;
  }

  dimension: barcode_event_slot_max {
    type: string
    sql: ${TABLE}.barcode_event_slot_max ;;
  }

  dimension: barcode_event_slot_min {
    type: string
    sql: ${TABLE}.barcode_event_slot_min ;;
  }

  dimension: barcode_key_a {
    type: string
    sql: ${TABLE}.barcode_key_a ;;
  }

  dimension: barcode_key_b {
    type: string
    sql: ${TABLE}.barcode_key_b ;;
  }

  dimension: barcode_key_c {
    type: string
    sql: ${TABLE}.barcode_key_c ;;
  }

  dimension: barcode_max_seats_per_row {
    type: string
    sql: ${TABLE}.barcode_max_seats_per_row ;;
  }

  dimension: barcode_status {
    type: string
    sql: ${TABLE}.barcode_status ;;
  }

  dimension: bootstrap {
    type: yesno
    sql: ${TABLE}.bootstrap ;;
  }

  dimension: capacity_upd_datetime {
    type: string
    sql: ${TABLE}.capacity_upd_datetime ;;
  }

  dimension: card_printing_enabled {
    type: string
    sql: ${TABLE}.card_printing_enabled ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: default_card_template_id {
    type: string
    sql: ${TABLE}.default_card_template_id ;;
  }

  dimension: default_ticket_template_id {
    type: string
    sql: ${TABLE}.default_ticket_template_id ;;
  }

  dimension: display_on_inet {
    type: string
    sql: ${TABLE}.display_on_inet ;;
  }

  dimension: display_status {
    type: string
    sql: ${TABLE}.display_status ;;
  }

  dimension: dsfp_same_num_seats_max {
    type: string
    sql: ${TABLE}.dsfp_same_num_seats_max ;;
  }

  dimension: dsfp_same_num_seats_min {
    type: string
    sql: ${TABLE}.dsfp_same_num_seats_min ;;
  }

  dimension: dsfp_same_num_seats_reqd {
    type: string
    sql: ${TABLE}.dsfp_same_num_seats_reqd ;;
  }

  dimension: dsfp_same_pricing_reqd {
    type: string
    sql: ${TABLE}.dsfp_same_pricing_reqd ;;
  }

  dimension: dsfp_same_ticket_type_reqd {
    type: string
    sql: ${TABLE}.dsfp_same_ticket_type_reqd ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}.dsn ;;
  }

  dimension: dsps_allowed {
    type: string
    sql: ${TABLE}.dsps_allowed ;;
  }

  dimension: duration {
    type: string
    sql: ${TABLE}.duration ;;
  }

  dimension: dw_event_id {
    type: string
    sql: ${TABLE}.dw_event_id ;;
  }

  dimension: dynamic_map_enabled {
    type: string
    sql: ${TABLE}.dynamic_map_enabled ;;
  }

  dimension: ee_id_source {
    type: string
    sql: ${TABLE}.ee_id_source ;;
  }

  dimension: enabled {
    type: string
    sql: ${TABLE}.enabled ;;
  }

  dimension: entitlements {
    type: string
    sql: ${TABLE}.entitlements ;;
  }

  dimension: event_abv {
    type: string
    sql: ${TABLE}.event_abv ;;
  }

  dimension: event_date {
    type: string
    sql: ${TABLE}.event_date ;;
  }

  dimension: event_day {
    type: string
    sql: ${TABLE}.event_day ;;
  }

  dimension: event_id {
    type: string
    sql: ${TABLE}.event_id ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: event_name_long {
    type: string
    sql: ${TABLE}.event_name_long ;;
  }

  dimension: event_sort {
    type: string
    sql: ${TABLE}.event_sort ;;
  }

  dimension: event_status {
    type: string
    sql: ${TABLE}.event_status ;;
  }

  dimension: event_time {
    type: string
    sql: ${TABLE}.event_time ;;
  }

  dimension: exchange_price_opt {
    type: string
    sql: ${TABLE}.exchange_price_opt ;;
  }

  dimension: expand_atomic {
    type: string
    sql: ${TABLE}.expand_atomic ;;
  }

  dimension: fantm_event_name {
    type: string
    sql: ${TABLE}.fantm_event_name ;;
  }

  dimension: forecast_enabled {
    type: string
    sql: ${TABLE}.forecast_enabled ;;
  }

  dimension: fp_max_seats {
    type: string
    sql: ${TABLE}.fp_max_seats ;;
  }

  dimension: fp_min_seats {
    type: string
    sql: ${TABLE}.fp_min_seats ;;
  }

  dimension: fror_alert_text {
    type: string
    sql: ${TABLE}.fror_alert_text ;;
  }

  dimension: fror_assignment_type {
    type: string
    sql: ${TABLE}.fror_assignment_type ;;
  }

  dimension: fror_max_parking {
    type: string
    sql: ${TABLE}.fror_max_parking ;;
  }

  dimension: fror_max_seats {
    type: string
    sql: ${TABLE}.fror_max_seats ;;
  }

  dimension: fror_min_seats {
    type: string
    sql: ${TABLE}.fror_min_seats ;;
  }

  dimension: fror_offsale_datetime {
    type: string
    sql: ${TABLE}.fror_offsale_datetime ;;
  }

  dimension: fror_onsale_datetime {
    type: string
    sql: ${TABLE}.fror_onsale_datetime ;;
  }

  dimension: fror_seat_list_id {
    type: string
    sql: ${TABLE}.fror_seat_list_id ;;
  }

  dimension: fse {
    type: string
    sql: ${TABLE}.fse ;;
  }

  dimension: full_price_ticket_type_code {
    type: string
    sql: ${TABLE}.full_price_ticket_type_code ;;
  }

  dimension: game_number {
    type: string
    sql: ${TABLE}.game_number ;;
  }

  dimension: geometry_file_id {
    type: string
    sql: ${TABLE}.geometry_file_id ;;
  }

  dimension: host_9_hold_enabled {
    type: string
    sql: ${TABLE}.host_9_hold_enabled ;;
  }

  dimension: host_activity_enabled {
    type: string
    sql: ${TABLE}.host_activity_enabled ;;
  }

  dimension: host_integration_enabled {
    type: string
    sql: ${TABLE}.host_integration_enabled ;;
  }

  dimension: host_name {
    type: string
    sql: ${TABLE}.host_name ;;
  }

  dimension: host_tcodes {
    type: string
    sql: ${TABLE}.host_tcodes ;;
  }

  dimension: inet_ada_count {
    type: string
    sql: ${TABLE}.inet_ada_count ;;
  }

  dimension: inet_ada_lm_ind {
    type: string
    sql: ${TABLE}.inet_ada_lm_ind ;;
  }

  dimension: inet_ada_sh_ind {
    type: string
    sql: ${TABLE}.inet_ada_sh_ind ;;
  }

  dimension: inet_ada_wc_ind {
    type: string
    sql: ${TABLE}.inet_ada_wc_ind ;;
  }

  dimension: inet_buy_method {
    type: string
    sql: ${TABLE}.inet_buy_method ;;
  }

  dimension: inet_display_datetime {
    type: string
    sql: ${TABLE}.inet_display_datetime ;;
  }

  dimension: inet_event_date_override {
    type: string
    sql: ${TABLE}.inet_event_date_override ;;
  }

  dimension: inet_event_description {
    type: string
    sql: ${TABLE}.inet_event_description ;;
  }

  dimension: inet_event_name {
    type: string
    sql: ${TABLE}.inet_event_name ;;
  }

  dimension: inet_event_text {
    type: string
    sql: ${TABLE}.inet_event_text ;;
  }

  dimension: inet_event_time_override {
    type: string
    sql: ${TABLE}.inet_event_time_override ;;
  }

  dimension: inet_exchange_method {
    type: string
    sql: ${TABLE}.inet_exchange_method ;;
  }

  dimension: inet_forwarding_allowed {
    type: string
    sql: ${TABLE}.inet_forwarding_allowed ;;
  }

  dimension: inet_line1 {
    type: string
    sql: ${TABLE}.inet_line1 ;;
  }

  dimension: inet_line10 {
    type: string
    sql: ${TABLE}.inet_line10 ;;
  }

  dimension: inet_line2 {
    type: string
    sql: ${TABLE}.inet_line2 ;;
  }

  dimension: inet_line3 {
    type: string
    sql: ${TABLE}.inet_line3 ;;
  }

  dimension: inet_line4 {
    type: string
    sql: ${TABLE}.inet_line4 ;;
  }

  dimension: inet_line5 {
    type: string
    sql: ${TABLE}.inet_line5 ;;
  }

  dimension: inet_line6 {
    type: string
    sql: ${TABLE}.inet_line6 ;;
  }

  dimension: inet_line7 {
    type: string
    sql: ${TABLE}.inet_line7 ;;
  }

  dimension: inet_line8 {
    type: string
    sql: ${TABLE}.inet_line8 ;;
  }

  dimension: inet_line9 {
    type: string
    sql: ${TABLE}.inet_line9 ;;
  }

  dimension: inet_print_offset {
    type: string
    sql: ${TABLE}.inet_print_offset ;;
  }

  dimension: inet_resale_allowed {
    type: string
    sql: ${TABLE}.inet_resale_allowed ;;
  }

  dimension: inet_rule_id {
    type: string
    sql: ${TABLE}.inet_rule_id ;;
  }

  dimension: inet_single_seats_option {
    type: string
    sql: ${TABLE}.inet_single_seats_option ;;
  }

  dimension: inet_soldout_opt {
    type: string
    sql: ${TABLE}.inet_soldout_opt ;;
  }

  dimension: inet_upgrade_method {
    type: string
    sql: ${TABLE}.inet_upgrade_method ;;
  }

  dimension: iris_validation {
    type: string
    sql: ${TABLE}.iris_validation ;;
  }

  dimension: ism_enabled {
    type: string
    sql: ${TABLE}.ism_enabled ;;
  }

  dimension: ism_map_override_id {
    type: string
    sql: ${TABLE}.ism_map_override_id ;;
  }

  dimension: ism_map_override_type {
    type: string
    sql: ${TABLE}.ism_map_override_type ;;
  }

  dimension: jds_mobile_enabled {
    type: string
    sql: ${TABLE}.jds_mobile_enabled ;;
  }

  dimension: ledger_id {
    type: string
    sql: ${TABLE}.ledger_id ;;
  }

  dimension: line1 {
    type: string
    sql: ${TABLE}.line1 ;;
  }

  dimension: line10 {
    type: string
    sql: ${TABLE}.line10 ;;
  }

  dimension: line2 {
    type: string
    sql: ${TABLE}.line2 ;;
  }

  dimension: line3 {
    type: string
    sql: ${TABLE}.line3 ;;
  }

  dimension: line4 {
    type: string
    sql: ${TABLE}.line4 ;;
  }

  dimension: line5 {
    type: string
    sql: ${TABLE}.line5 ;;
  }

  dimension: line6 {
    type: string
    sql: ${TABLE}.line6 ;;
  }

  dimension: line7 {
    type: string
    sql: ${TABLE}.line7 ;;
  }

  dimension: line8 {
    type: string
    sql: ${TABLE}.line8 ;;
  }

  dimension: line9 {
    type: string
    sql: ${TABLE}.line9 ;;
  }

  dimension: long_barcode_key {
    type: string
    sql: ${TABLE}.long_barcode_key ;;
  }

  dimension: major_category {
    type: string
    sql: ${TABLE}.major_category ;;
  }

  dimension: mask_event {
    type: string
    sql: ${TABLE}.mask_event ;;
  }

  dimension: membership_reqd {
    type: string
    sql: ${TABLE}.membership_reqd ;;
  }

  dimension: min_events {
    type: string
    sql: ${TABLE}.min_events ;;
  }

  dimension: minor_category {
    type: string
    sql: ${TABLE}.minor_category ;;
  }

  dimension: mobile_template_id {
    type: string
    sql: ${TABLE}.mobile_template_id ;;
  }

  dimension: offsale_datetime {
    type: string
    sql: ${TABLE}.offsale_datetime ;;
  }

  dimension: onsale_datetime {
    type: string
    sql: ${TABLE}.onsale_datetime ;;
  }

  dimension: oss_offsale_datetime {
    type: string
    sql: ${TABLE}.oss_offsale_datetime ;;
  }

  dimension: oss_onsale_datetime {
    type: string
    sql: ${TABLE}.oss_onsale_datetime ;;
  }

  dimension: pay_first_on_payment_plan_ind {
    type: string
    sql: ${TABLE}.pay_first_on_payment_plan_ind ;;
  }

  dimension: pc_membership_reqd {
    type: string
    sql: ${TABLE}.pc_membership_reqd ;;
  }

  dimension: pid_mult_tickets_allowed {
    type: string
    sql: ${TABLE}.pid_mult_tickets_allowed ;;
  }

  dimension: plan {
    type: string
    sql: ${TABLE}.plan ;;
  }

  dimension: plan_abv {
    type: string
    sql: ${TABLE}.plan_abv ;;
  }

  dimension: plan_discount_method {
    type: string
    sql: ${TABLE}.plan_discount_method ;;
  }

  dimension: plan_id {
    type: string
    sql: ${TABLE}.plan_id ;;
  }

  dimension: plan_level_print_ind {
    type: string
    sql: ${TABLE}.plan_level_print_ind ;;
  }

  dimension: plan_name {
    type: string
    sql: ${TABLE}.plan_name ;;
  }

  dimension: plan_type {
    type: string
    sql: ${TABLE}.plan_type ;;
  }

  dimension: primary_act {
    type: string
    sql: ${TABLE}.primary_act ;;
  }

  dimension: print_ticket_ind {
    type: string
    sql: ${TABLE}.print_ticket_ind ;;
  }

  dimension: requests_enabled {
    type: string
    sql: ${TABLE}.requests_enabled ;;
  }

  dimension: reservable {
    type: string
    sql: ${TABLE}.reservable ;;
  }

  dimension: ret_enabled {
    type: string
    sql: ${TABLE}.ret_enabled ;;
  }

  dimension: retail_enabled {
    type: string
    sql: ${TABLE}.retail_enabled ;;
  }

  dimension: retail_eventdb_ind {
    type: string
    sql: ${TABLE}.retail_eventdb_ind ;;
  }

  dimension: retail_process_opt {
    type: string
    sql: ${TABLE}.retail_process_opt ;;
  }

  dimension: retail_status {
    type: string
    sql: ${TABLE}.retail_status ;;
  }

  dimension: retail_substatus {
    type: string
    sql: ${TABLE}.retail_substatus ;;
  }

  dimension: returnable {
    type: string
    sql: ${TABLE}.returnable ;;
  }

  dimension: rfid_enabled {
    type: string
    sql: ${TABLE}.rfid_enabled ;;
  }

  dimension: rofr_buy_now_option {
    type: string
    sql: ${TABLE}.rofr_buy_now_option ;;
  }

  dimension: season_id {
    type: string
    sql: ${TABLE}.season_id ;;
  }

  dimension: seat_locator_event_line_1 {
    type: string
    sql: ${TABLE}.seat_locator_event_line_1 ;;
  }

  dimension: seat_locator_event_line_2 {
    type: string
    sql: ${TABLE}.seat_locator_event_line_2 ;;
  }

  dimension: seat_locator_line_1 {
    type: string
    sql: ${TABLE}.seat_locator_line_1 ;;
  }

  dimension: seat_locator_line_2 {
    type: string
    sql: ${TABLE}.seat_locator_line_2 ;;
  }

  dimension: seat_locator_line_3 {
    type: string
    sql: ${TABLE}.seat_locator_line_3 ;;
  }

  dimension: seat_query_plan_id {
    type: string
    sql: ${TABLE}.seat_query_plan_id ;;
  }

  dimension: secondary_act {
    type: string
    sql: ${TABLE}.secondary_act ;;
  }

  dimension: sell_rule_id {
    type: string
    sql: ${TABLE}.sell_rule_id ;;
  }

  dimension: surchg_enabled {
    type: string
    sql: ${TABLE}.surchg_enabled ;;
  }

  dimension: team {
    type: string
    sql: ${TABLE}.team ;;
  }

  dimension: tm_event_name {
    type: string
    sql: ${TABLE}.tm_event_name ;;
  }

  dimension: tm_onsale_datetime {
    type: string
    sql: ${TABLE}.tm_onsale_datetime ;;
  }

  dimension: tm_outlet_datetime {
    type: string
    sql: ${TABLE}.tm_outlet_datetime ;;
  }

  dimension: tm_phone_datetime {
    type: string
    sql: ${TABLE}.tm_phone_datetime ;;
  }

  dimension: tm_presale_off_datetime {
    type: string
    sql: ${TABLE}.tm_presale_off_datetime ;;
  }

  dimension: tm_presale_on_datetime {
    type: string
    sql: ${TABLE}.tm_presale_on_datetime ;;
  }

  dimension: total_events {
    type: string
    sql: ${TABLE}.total_events ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: upd_datetime {
    type: string
    sql: ${TABLE}.upd_datetime ;;
  }

  dimension: upd_user {
    type: string
    sql: ${TABLE}.upd_user ;;
  }

  dimension: upsell_soldout_display_option {
    type: string
    sql: ${TABLE}.upsell_soldout_display_option ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      emt_event_id,
      event_name,
      plan_name,
      tm_event_name,
      fantm_event_name,
      inet_event_name,
      host_name
    ]
  }
}
