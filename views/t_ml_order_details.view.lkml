view: t_ml_order_details {
  sql_table_name: safetix.t_ml_order_details ;;
  suggestions: no

  dimension: acct_guid {
    type: string
    sql: ${TABLE}.acct_guid ;;
  }

  dimension: acct_id {
    type: string
    sql: ${TABLE}.acct_id ;;
  }

  dimension: add_datetime {
    type: string
    sql: ${TABLE}.add_datetime ;;
  }

  dimension: block_purchase_price {
    type: string
    sql: ${TABLE}.block_purchase_price ;;
  }

  dimension: bootstrap {
    type: yesno
    sql: ${TABLE}.bootstrap ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}.dsn ;;
  }

  dimension: dw_event_id {
    type: string
    sql: ${TABLE}.dw_event_id ;;
  }

  dimension: event_date {
    type: string
    sql: ${TABLE}.event_date ;;
  }

  dimension: event_id {
    type: string
    sql: ${TABLE}.event_id ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: global_event_id {
    type: string
    sql: ${TABLE}.global_event_id ;;
  }

  dimension: last_seat {
    type: string
    sql: ${TABLE}.last_seat ;;
  }

  dimension: num_seats {
    type: string
    sql: ${TABLE}.num_seats ;;
  }

  dimension: order_date {
    type: string
    sql: ${TABLE}.order_date ;;
  }

  dimension: order_guid {
    type: string
    sql: ${TABLE}.order_guid ;;
  }

  dimension: order_line_item {
    type: string
    sql: ${TABLE}.order_line_item ;;
  }

  dimension: order_line_item_seq {
    type: string
    sql: ${TABLE}.order_line_item_seq ;;
  }

  dimension: order_num {
    type: string
    sql: ${TABLE}.order_num ;;
  }

  dimension: price_code {
    type: string
    sql: ${TABLE}.price_code ;;
  }

  dimension: purchase_price {
    type: string
    sql: ${TABLE}.purchase_price ;;
  }

  dimension: row_id {
    type: string
    sql: ${TABLE}.row_id ;;
  }

  dimension: row_name {
    type: string
    sql: ${TABLE}.row_name ;;
  }

  dimension: seat_increment {
    type: string
    sql: ${TABLE}.seat_increment ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}.seat_num ;;
  }

  dimension: secondary_seq_id {
    type: string
    sql: ${TABLE}.secondary_seq_id ;;
  }

  dimension: section_id {
    type: string
    sql: ${TABLE}.section_id ;;
  }

  dimension: section_name {
    type: string
    sql: ${TABLE}.section_name ;;
  }

  dimension: seq_id {
    type: string
    sql: ${TABLE}.seq_id ;;
  }

  dimension: ticket_seq_id {
    type: string
    sql: ${TABLE}.ticket_seq_id ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  measure: count {
    type: count
    drill_fields: [event_name, section_name, row_name]
  }
}
