view: t_ml_cust_demographics {
  sql_table_name: safetix.t_ml_cust_demographics ;;
  suggestions: no

  dimension: acct_id {
    type: string
    sql: ${TABLE}.acct_id ;;
  }

  dimension: bootstrap {
    type: yesno
    sql: ${TABLE}.bootstrap ;;
  }

  dimension: cust_name_id {
    type: string
    sql: ${TABLE}.cust_name_id ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}.dsn ;;
  }

  dimension: email_addr {
    type: string
    sql: ${TABLE}.email_addr ;;
  }

  dimension: full_name {
    type: string
    sql: ${TABLE}.full_name ;;
  }

  dimension: login_count {
    type: string
    sql: ${TABLE}.login_count ;;
  }

  dimension: object_id {
    type: string
    sql: ${TABLE}.object_id ;;
  }

  dimension: phone_number {
    type: string
    sql: ${TABLE}.phone_number ;;
  }

  dimension: pin {
    type: string
    sql: ${TABLE}.pin ;;
  }

  dimension: seq_id {
    type: string
    sql: ${TABLE}.seq_id ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: upd_datetime {
    type: string
    sql: ${TABLE}.upd_datetime ;;
  }

  measure: count {
    type: count
    drill_fields: [full_name]
  }

  measure: Value {

    type: number
    sql: count(*) ;;
  }
}
