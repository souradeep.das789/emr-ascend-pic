view: t_ticket {
  sql_table_name: safetix.t_ticket ;;
  suggestions: no

  dimension: account_change_datetime {
    type: string
    sql: ${TABLE}.account_change_datetime ;;
  }

  dimension: acct_blk_id {
    type: string
    sql: ${TABLE}.acct_blk_id ;;
  }

  dimension: acct_id {
    type: string
    sql: ${TABLE}.acct_id ;;
  }

  dimension: acct_rep_id {
    type: string
    sql: ${TABLE}.acct_rep_id ;;
  }

  dimension: acct_seq_id {
    type: string
    sql: ${TABLE}.acct_seq_id ;;
  }

  dimension: acs_marks_sent {
    type: string
    sql: ${TABLE}.acs_marks_sent ;;
  }

  dimension: add_datetime {
    type: string
    sql: ${TABLE}.add_datetime ;;
  }

  dimension: add_usr {
    type: string
    sql: ${TABLE}.add_usr ;;
  }

  dimension: aisle {
    type: string
    sql: ${TABLE}.aisle ;;
  }

  dimension: assoc_acct_id {
    type: string
    sql: ${TABLE}.assoc_acct_id ;;
  }

  dimension: block_full_price {
    type: string
    sql: ${TABLE}.block_full_price ;;
  }

  dimension: block_purchase_price {
    type: string
    sql: ${TABLE}.block_purchase_price ;;
  }

  dimension: bootstrap {
    type: yesno
    sql: ${TABLE}.bootstrap ;;
  }

  dimension: class_id {
    type: string
    sql: ${TABLE}.class_id ;;
  }

  dimension: club_group_id {
    type: string
    sql: ${TABLE}.club_group_id ;;
  }

  dimension: comp_approved_by {
    type: string
    sql: ${TABLE}.comp_approved_by ;;
  }

  dimension: comp_code {
    type: string
    sql: ${TABLE}.comp_code ;;
  }

  dimension: comp_comment {
    type: string
    sql: ${TABLE}.comp_comment ;;
  }

  dimension: comp_requested_by {
    type: string
    sql: ${TABLE}.comp_requested_by ;;
  }

  dimension: consignment {
    type: string
    sql: ${TABLE}.consignment ;;
  }

  dimension: contract_id {
    type: string
    sql: ${TABLE}.contract_id ;;
  }

  dimension: cust_name_id {
    type: string
    sql: ${TABLE}.cust_name_id ;;
  }

  dimension: delivered_date {
    type: string
    sql: ${TABLE}.delivered_date ;;
  }

  dimension: delivery_addr1 {
    type: string
    sql: ${TABLE}.delivery_addr1 ;;
  }

  dimension: delivery_addr2 {
    type: string
    sql: ${TABLE}.delivery_addr2 ;;
  }

  dimension: delivery_addr3 {
    type: string
    sql: ${TABLE}.delivery_addr3 ;;
  }

  dimension: delivery_city {
    type: string
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: delivery_country {
    type: string
    sql: ${TABLE}.delivery_country ;;
  }

  dimension: delivery_instructions {
    type: string
    sql: ${TABLE}.delivery_instructions ;;
  }

  dimension: delivery_method {
    type: string
    sql: ${TABLE}.delivery_method ;;
  }

  dimension: delivery_name_first {
    type: string
    sql: ${TABLE}.delivery_name_first ;;
  }

  dimension: delivery_name_last {
    type: string
    sql: ${TABLE}.delivery_name_last ;;
  }

  dimension: delivery_phone {
    type: string
    sql: ${TABLE}.delivery_phone ;;
  }

  dimension: delivery_state {
    type: string
    sql: ${TABLE}.delivery_state ;;
  }

  dimension: delivery_zip {
    type: string
    sql: ${TABLE}.delivery_zip ;;
  }

  dimension: disc_amount {
    type: string
    sql: ${TABLE}.disc_amount ;;
  }

  dimension: disc_code {
    type: string
    sql: ${TABLE}.disc_code ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}.dsn ;;
  }

  dimension: entitlements_used {
    type: string
    sql: ${TABLE}.entitlements_used ;;
  }

  dimension: event_id {
    type: string
    sql: ${TABLE}.event_id ;;
  }

  dimension: full_price {
    type: string
    sql: ${TABLE}.full_price ;;
  }

  dimension: group_flag {
    type: string
    sql: ${TABLE}.group_flag ;;
  }

  dimension: group_sales_id {
    type: string
    sql: ${TABLE}.group_sales_id ;;
  }

  dimension: hold_source {
    type: string
    sql: ${TABLE}.hold_source ;;
  }

  dimension: host_synch_status {
    type: string
    sql: ${TABLE}.host_synch_status ;;
  }

  dimension: inet_cc_seq_num {
    type: string
    sql: ${TABLE}.inet_cc_seq_num ;;
  }

  dimension: inet_delivery_method {
    type: string
    sql: ${TABLE}.inet_delivery_method ;;
  }

  dimension: inet_fraud_score_code {
    type: string
    sql: ${TABLE}.inet_fraud_score_code ;;
  }

  dimension: inet_purchase_price {
    type: string
    sql: ${TABLE}.inet_purchase_price ;;
  }

  dimension: inet_status {
    type: string
    sql: ${TABLE}.inet_status ;;
  }

  dimension: membership_cust_membership_id {
    type: string
    sql: ${TABLE}.membership_cust_membership_id ;;
  }

  dimension: num_seats {
    type: string
    sql: ${TABLE}.num_seats ;;
  }

  dimension: offer_id {
    type: string
    sql: ${TABLE}.offer_id ;;
  }

  dimension: order_guid {
    type: string
    sql: ${TABLE}.order_guid ;;
  }

  dimension: order_line_item {
    type: string
    sql: ${TABLE}.order_line_item ;;
  }

  dimension: order_line_item_seq {
    type: string
    sql: ${TABLE}.order_line_item_seq ;;
  }

  dimension: order_num {
    type: string
    sql: ${TABLE}.order_num ;;
  }

  dimension: orig_datetime {
    type: string
    sql: ${TABLE}.orig_datetime ;;
  }

  dimension: orig_event_id {
    type: string
    sql: ${TABLE}.orig_event_id ;;
  }

  dimension: orig_price_code {
    type: string
    sql: ${TABLE}.orig_price_code ;;
  }

  dimension: orig_user {
    type: string
    sql: ${TABLE}.orig_user ;;
  }

  dimension: other_info_1 {
    type: string
    sql: ${TABLE}.other_info_1 ;;
  }

  dimension: other_info_10 {
    type: string
    sql: ${TABLE}.other_info_10 ;;
  }

  dimension: other_info_2 {
    type: string
    sql: ${TABLE}.other_info_2 ;;
  }

  dimension: other_info_3 {
    type: string
    sql: ${TABLE}.other_info_3 ;;
  }

  dimension: other_info_4 {
    type: string
    sql: ${TABLE}.other_info_4 ;;
  }

  dimension: other_info_5 {
    type: string
    sql: ${TABLE}.other_info_5 ;;
  }

  dimension: other_info_6 {
    type: string
    sql: ${TABLE}.other_info_6 ;;
  }

  dimension: other_info_7 {
    type: string
    sql: ${TABLE}.other_info_7 ;;
  }

  dimension: other_info_8 {
    type: string
    sql: ${TABLE}.other_info_8 ;;
  }

  dimension: other_info_9 {
    type: string
    sql: ${TABLE}.other_info_9 ;;
  }

  dimension: pc_licfee {
    type: string
    sql: ${TABLE}.pc_licfee ;;
  }

  dimension: pc_other1 {
    type: string
    sql: ${TABLE}.pc_other1 ;;
  }

  dimension: pc_other2 {
    type: string
    sql: ${TABLE}.pc_other2 ;;
  }

  dimension: pc_other3 {
    type: string
    sql: ${TABLE}.pc_other3 ;;
  }

  dimension: pc_other4 {
    type: string
    sql: ${TABLE}.pc_other4 ;;
  }

  dimension: pc_other5 {
    type: string
    sql: ${TABLE}.pc_other5 ;;
  }

  dimension: pc_other6 {
    type: string
    sql: ${TABLE}.pc_other6 ;;
  }

  dimension: pc_other7 {
    type: string
    sql: ${TABLE}.pc_other7 ;;
  }

  dimension: pc_other8 {
    type: string
    sql: ${TABLE}.pc_other8 ;;
  }

  dimension: pc_tax {
    type: string
    sql: ${TABLE}.pc_tax ;;
  }

  dimension: pc_ticket {
    type: string
    sql: ${TABLE}.pc_ticket ;;
  }

  dimension: pid_id {
    type: string
    sql: ${TABLE}.pid_id ;;
  }

  dimension: plan_datetime {
    type: string
    sql: ${TABLE}.plan_datetime ;;
  }

  dimension: plan_event_id {
    type: string
    sql: ${TABLE}.plan_event_id ;;
  }

  dimension: plan_seq_id {
    type: string
    sql: ${TABLE}.plan_seq_id ;;
  }

  dimension: plan_type {
    type: string
    sql: ${TABLE}.plan_type ;;
  }

  dimension: posted_by {
    type: string
    sql: ${TABLE}.posted_by ;;
  }

  dimension: posting_exp_datetime {
    type: string
    sql: ${TABLE}.posting_exp_datetime ;;
  }

  dimension: posting_price {
    type: string
    sql: ${TABLE}.posting_price ;;
  }

  dimension: posting_pricing_method {
    type: string
    sql: ${TABLE}.posting_pricing_method ;;
  }

  dimension: posting_source {
    type: string
    sql: ${TABLE}.posting_source ;;
  }

  dimension: prev_loc_id {
    type: string
    sql: ${TABLE}.prev_loc_id ;;
  }

  dimension: price_code {
    type: string
    sql: ${TABLE}.price_code ;;
  }

  dimension: pricing_method {
    type: string
    sql: ${TABLE}.pricing_method ;;
  }

  dimension: printed {
    type: string
    sql: ${TABLE}.printed ;;
  }

  dimension: printed_datetime {
    type: string
    sql: ${TABLE}.printed_datetime ;;
  }

  dimension: printed_price {
    type: string
    sql: ${TABLE}.printed_price ;;
  }

  dimension: promo_code_id {
    type: string
    sql: ${TABLE}.promo_code_id ;;
  }

  dimension: purchase_price {
    type: string
    sql: ${TABLE}.purchase_price ;;
  }

  dimension: renewal_ind {
    type: string
    sql: ${TABLE}.renewal_ind ;;
  }

  dimension: request_line_item {
    type: string
    sql: ${TABLE}.request_line_item ;;
  }

  dimension: resale_system {
    type: string
    sql: ${TABLE}.resale_system ;;
  }

  dimension: reserved_datetime {
    type: string
    sql: ${TABLE}.reserved_datetime ;;
  }

  dimension: reserved_ind {
    type: string
    sql: ${TABLE}.reserved_ind ;;
  }

  dimension: resold_ind {
    type: string
    sql: ${TABLE}.resold_ind ;;
  }

  dimension: retail_acct_add_date {
    type: string
    sql: ${TABLE}.retail_acct_add_date ;;
  }

  dimension: retail_acct_num {
    type: string
    sql: ${TABLE}.retail_acct_num ;;
  }

  dimension: retail_distance_charge {
    type: string
    sql: ${TABLE}.retail_distance_charge ;;
  }

  dimension: retail_face_value {
    type: string
    sql: ${TABLE}.retail_face_value ;;
  }

  dimension: retail_face_value_tax {
    type: string
    sql: ${TABLE}.retail_face_value_tax ;;
  }

  dimension: retail_facility_fee {
    type: string
    sql: ${TABLE}.retail_facility_fee ;;
  }

  dimension: retail_mask {
    type: string
    sql: ${TABLE}.retail_mask ;;
  }

  dimension: retail_mop {
    type: string
    sql: ${TABLE}.retail_mop ;;
  }

  dimension: retail_price_level {
    type: string
    sql: ${TABLE}.retail_price_level ;;
  }

  dimension: retail_service_charge {
    type: string
    sql: ${TABLE}.retail_service_charge ;;
  }

  dimension: retail_service_charge_tax {
    type: string
    sql: ${TABLE}.retail_service_charge_tax ;;
  }

  dimension: retail_system_name {
    type: string
    sql: ${TABLE}.retail_system_name ;;
  }

  dimension: retail_ticket_type {
    type: string
    sql: ${TABLE}.retail_ticket_type ;;
  }

  dimension: return_class_id {
    type: string
    sql: ${TABLE}.return_class_id ;;
  }

  dimension: row_id {
    type: string
    sql: ${TABLE}.row_id ;;
  }

  dimension: sales_source_date {
    type: string
    sql: ${TABLE}.sales_source_date ;;
  }

  dimension: sales_source_direct_ind {
    type: string
    sql: ${TABLE}.sales_source_direct_ind ;;
  }

  dimension: sales_source_id {
    type: string
    sql: ${TABLE}.sales_source_id ;;
  }

  dimension: seat_increment {
    type: string
    sql: ${TABLE}.seat_increment ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}.seat_num ;;
  }

  dimension: section_id {
    type: string
    sql: ${TABLE}.section_id ;;
  }

  dimension: sell_location_id {
    type: string
    sql: ${TABLE}.sell_location_id ;;
  }

  dimension: sold_zip {
    type: string
    sql: ${TABLE}.sold_zip ;;
  }

  dimension: sub_plan_event_id {
    type: string
    sql: ${TABLE}.sub_plan_event_id ;;
  }

  dimension: sub_plan_seq_id {
    type: string
    sql: ${TABLE}.sub_plan_seq_id ;;
  }

  dimension: surchg_amount {
    type: string
    sql: ${TABLE}.surchg_amount ;;
  }

  dimension: surchg_code {
    type: string
    sql: ${TABLE}.surchg_code ;;
  }

  dimension: target_trans_id {
    type: string
    sql: ${TABLE}.target_trans_id ;;
  }

  dimension: target_trans_type {
    type: string
    sql: ${TABLE}.target_trans_type ;;
  }

  dimension: ticket_seq_id {
    type: string
    sql: ${TABLE}.ticket_seq_id ;;
  }

  dimension: ticket_type_code {
    type: string
    sql: ${TABLE}.ticket_type_code ;;
  }

  dimension: tmr_ticket_id {
    type: string
    sql: ${TABLE}.tmr_ticket_id ;;
  }

  dimension: total_events {
    type: string
    sql: ${TABLE}.total_events ;;
  }

  dimension: trans_type {
    type: string
    sql: ${TABLE}.trans_type ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: upd_datetime {
    type: string
    sql: ${TABLE}.upd_datetime ;;
  }

  dimension: upd_user {
    type: string
    sql: ${TABLE}.upd_user ;;
  }

  dimension: used_cust_membership_id {
    type: string
    sql: ${TABLE}.used_cust_membership_id ;;
  }

  measure: count {
    type: count
    drill_fields: [retail_system_name]
  }
}
