view: emr_ascend {
  sql_table_name: order_ticket.emr_ascend ;;
  suggestions: no

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: o_day {
    type: number
    sql: ${TABLE}.o_day ;;
  }

  dimension: o_ordernumber {
    type: string
    sql: ${TABLE}.o_ordernumber ;;
  }

  dimension: o_svalue {
    type: string
    sql: ${TABLE}.o_svalue ;;
  }

  dimension_group: o {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.o_time ;;
  }

  dimension: ocode {
    type: string
    sql: ${TABLE}.ocode ;;
  }

  dimension: t_day {
    type: string
    sql: ${TABLE}.t_day ;;
  }

  dimension: t_ordernumber {
    type: string
    sql: ${TABLE}.t_ordernumber ;;
  }

  dimension: t_svalue {
    type: string
    sql: ${TABLE}.t_svalue ;;
  }

  dimension_group: t {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.t_time ;;
  }

  dimension: valid_email {
    type: yesno
    sql: ${TABLE}.valid_email ;;
  }

  dimension: value {
    type: string
    sql: ${TABLE}.value ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
