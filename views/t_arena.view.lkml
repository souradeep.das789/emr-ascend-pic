view: t_arena {
  sql_table_name: safetix.t_arena ;;
  suggestions: no

  dimension: apply_host_rules {
    type: string
    sql: ${TABLE}.apply_host_rules ;;
  }

  dimension: arena_abv {
    type: string
    sql: ${TABLE}.arena_abv ;;
  }

  dimension: arena_id {
    type: string
    sql: ${TABLE}.arena_id ;;
  }

  dimension: arena_name {
    type: string
    sql: ${TABLE}.arena_name ;;
  }

  dimension: bootstrap {
    type: yesno
    sql: ${TABLE}.bootstrap ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: digit_server_id {
    type: string
    sql: ${TABLE}.digit_server_id ;;
  }

  dimension: donotupdate_tz_name {
    type: string
    sql: ${TABLE}.donotupdate_tz_name ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}.dsn ;;
  }

  dimension: host_name {
    type: string
    sql: ${TABLE}.host_name ;;
  }

  dimension: host_tcode {
    type: string
    sql: ${TABLE}.host_tcode ;;
  }

  dimension: im_mode {
    type: string
    sql: ${TABLE}.im_mode ;;
  }

  dimension: inet_arena_name {
    type: string
    sql: ${TABLE}.inet_arena_name ;;
  }

  dimension: phone {
    type: string
    sql: ${TABLE}.phone ;;
  }

  dimension: scoreboard_height {
    type: string
    sql: ${TABLE}.scoreboard_height ;;
  }

  dimension: scoreboard_width {
    type: string
    sql: ${TABLE}.scoreboard_width ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  dimension: street_addr_1 {
    type: string
    sql: ${TABLE}.street_addr_1 ;;
  }

  dimension: street_addr_2 {
    type: string
    sql: ${TABLE}.street_addr_2 ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: tz_id {
    type: string
    sql: ${TABLE}.tz_id ;;
  }

  dimension: venue_abv {
    type: string
    sql: ${TABLE}.venue_abv ;;
  }

  dimension: venue_altitude {
    type: string
    sql: ${TABLE}.venue_altitude ;;
  }

  dimension: venue_city {
    type: string
    sql: ${TABLE}.venue_city ;;
  }

  dimension: venue_latitude {
    type: string
    sql: ${TABLE}.venue_latitude ;;
  }

  dimension: venue_longitude {
    type: string
    sql: ${TABLE}.venue_longitude ;;
  }

  dimension: venue_number {
    type: string
    sql: ${TABLE}.venue_number ;;
  }

  dimension: venue_source {
    type: string
    sql: ${TABLE}.venue_source ;;
  }

  dimension: venue_state {
    type: string
    sql: ${TABLE}.venue_state ;;
  }

  dimension: venue_zip {
    type: string
    sql: ${TABLE}.venue_zip ;;
  }

  dimension: zip {
    type: zipcode
    sql: ${TABLE}.zip ;;
  }

  measure: count {
    type: count
    drill_fields: [arena_name, host_name, inet_arena_name, donotupdate_tz_name]
  }
}
