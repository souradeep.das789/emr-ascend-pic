view: ascend {
  sql_table_name: order_ticket.ascend ;;
  suggestions: no

  dimension: o_billingaddress_zip {
    type: string
    sql: ${TABLE}.o_billingaddress_zip ;;
  }

  dimension: o_buyer_email {
    type: string
    sql: ${TABLE}.o_buyer_email ;;
  }

  dimension: o_kafka_timestamp {
    type: string
    sql: ${TABLE}.o_kafka_timestamp ;;
  }

  dimension: o_price_total {
    type: number
    sql: ${TABLE}.o_price_total ;;
  }

  dimension: t_entryportal {
    type: string
    sql: ${TABLE}.t_entryportal ;;
  }

  dimension: t_eventdatems {
    type: string
    sql: ${TABLE}.t_eventdatems ;;
  }

  dimension: t_eventid {
    type: string
    sql: ${TABLE}.t_eventid ;;
  }

  dimension: t_kafka_timestamp {
    type: string
    sql: ${TABLE}.t_kafka_timestamp ;;
  }

  dimension: t_offername {
    type: string
    sql: ${TABLE}.t_offername ;;
  }

  dimension: t_orderid {
    type: string
    sql: ${TABLE}.t_orderid ;;
  }

  dimension: t_ordernumber {
    type: string
    sql: ${TABLE}.t_ordernumber ;;
  }

  dimension: t_partition_timestamp {
    type: string
    sql: ${TABLE}.t_partition_timestamp ;;
  }

  dimension: t_placeid {
    type: string
    sql: ${TABLE}.t_placeid ;;
  }

  dimension: t_price {
    type: string
    sql: ${TABLE}.t_price ;;
  }

  dimension: t_rowname {
    type: string
    sql: ${TABLE}.t_rowname ;;
  }

  dimension: t_seatname {
    type: string
    sql: ${TABLE}.t_seatname ;;
  }

  dimension: t_sectionname {
    type: string
    sql: ${TABLE}.t_sectionname ;;
  }

  dimension: t_source {
    type: string
    sql: ${TABLE}.t_source ;;
  }

  dimension: t_sourcetimestamp {
    type: string
    sql: ${TABLE}.t_sourcetimestamp ;;
  }

  dimension: t_sourceversion {
    type: string
    sql: ${TABLE}.t_sourceversion ;;
  }

  dimension: t_timestamp {
    type: string
    sql: ${TABLE}.t_timestamp ;;
  }

  dimension: t_type {
    type: string
    sql: ${TABLE}.t_type ;;
  }

  dimension: t_valid {
    type: string
    sql: ${TABLE}.t_valid ;;
  }
  dimension_group: datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.t_sourcetimestamp ;;
  }
  measure: count {
    type: count
    drill_fields: [t_sectionname, t_rowname, t_seatname, t_offername]
  }
}
