view: t_ticket_barcode {
  sql_table_name: safetix.t_ticket_barcode ;;
  suggestions: no

  dimension: acct_id {
    type: string
    sql: ${TABLE}.acct_id ;;
  }

  dimension: add_datetime {
    type: string
    sql: ${TABLE}.add_datetime ;;
  }

  dimension: add_user {
    type: string
    sql: ${TABLE}.add_user ;;
  }

  dimension: alt_barcode {
    type: string
    sql: ${TABLE}.alt_barcode ;;
  }

  dimension: amb_reset_datetime {
    type: string
    sql: ${TABLE}.amb_reset_datetime ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: bc_event_id {
    type: string
    sql: ${TABLE}.bc_event_id ;;
  }

  dimension: bc_row_id {
    type: string
    sql: ${TABLE}.bc_row_id ;;
  }

  dimension: bc_seat_num {
    type: string
    sql: ${TABLE}.bc_seat_num ;;
  }

  dimension: bc_section_id {
    type: string
    sql: ${TABLE}.bc_section_id ;;
  }

  dimension: bootstrap {
    type: yesno
    sql: ${TABLE}.bootstrap ;;
  }

  dimension: channel_ind {
    type: string
    sql: ${TABLE}.channel_ind ;;
  }

  dimension: dsn {
    type: string
    sql: ${TABLE}.dsn ;;
  }

  dimension: event_id {
    type: string
    sql: ${TABLE}.event_id ;;
  }

  dimension: event_slot {
    type: string
    sql: ${TABLE}.event_slot ;;
  }

  dimension: ext_post_ind {
    type: string
    sql: ${TABLE}.ext_post_ind ;;
  }

  dimension: ext_post_source {
    type: string
    sql: ${TABLE}.ext_post_source ;;
  }

  dimension: jds_id {
    type: string
    sql: ${TABLE}.jds_id ;;
  }

  dimension: long_barcode {
    type: string
    sql: ${TABLE}.long_barcode ;;
  }

  dimension: method {
    type: string
    sql: ${TABLE}.method ;;
  }

  dimension: mspr {
    type: string
    sql: ${TABLE}.mspr ;;
  }

  dimension: pid_id {
    type: string
    sql: ${TABLE}.pid_id ;;
  }

  dimension: plan_event_id {
    type: string
    sql: ${TABLE}.plan_event_id ;;
  }

  dimension: printed {
    type: string
    sql: ${TABLE}.printed ;;
  }

  dimension: purpose {
    type: string
    sql: ${TABLE}.purpose ;;
  }

  dimension: rbi {
    type: string
    sql: ${TABLE}.rbi ;;
  }

  dimension: row_id {
    type: string
    sql: ${TABLE}.row_id ;;
  }

  dimension: season_key {
    type: string
    sql: ${TABLE}.season_key ;;
  }

  dimension: seat_num {
    type: string
    sql: ${TABLE}.seat_num ;;
  }

  dimension: seatcode {
    type: string
    sql: ${TABLE}.seatcode ;;
  }

  dimension: section_id {
    type: string
    sql: ${TABLE}.section_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: system_id {
    type: string
    sql: ${TABLE}.system_id ;;
  }

  dimension: tf_id {
    type: string
    sql: ${TABLE}.tf_id ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: upd_datetime {
    type: string
    sql: ${TABLE}.upd_datetime ;;
  }

  dimension: upd_user {
    type: string
    sql: ${TABLE}.upd_user ;;
  }

              #                                          Calculated Dimension and Measure

  dimension: Cal_Fantm_Event_Name  {
    type: string
    sql: case when ${t_ticket_barcode.bc_event_id} IS NULL Then ${t_event.fantm_event_name}
    when ${t_ticket_barcode.event_id} = ${t_ticket_barcode.bc_event_id} then ${t_event.fantm_event_name}
    else  concat(${t_event.fantm_event_name} , '-', ${Temp_Fan_Event_Name})
    end;;
      }

dimension: Temp_Fan_Event_Name {
  type: string
  sql: case when ${t_event.event_id} = ${t_ticket_barcode.bc_event_id} then ${t_event.fantm_event_name}
  else ${t_event.event_name}
  end;;
}

  dimension: Is_Viewed_As_Ret {
    type: string
    sql: case when ${jds_id} IS NOT NULL AND ${channel_ind} NOT IN ('','1') AND ${t_event.ret_enabled} NOT IN ('','Y') Then 'Y'
        else 'N'
        end;;
  }


dimension: Is_Saved_To_Passbook {
  type: string
  sql: case when ${jds_id} IS NOT NULL AND ${channel_ind} NOT IN ('','1') then 'Y'
  else 'N'
  end;;
}
  dimension: Is_Printed {
    type: string
    sql: case when ${bc_event_id} IS NULL THEN 'N'
      else 'Y'
      end;;
  }




dimension: Is_Premptive {
  type: string
  sql: case when ${channel_ind} = '1' THEN 'Y'
      else 'N'
      end;;
}

dimension: is_printed_as_Pass_or_Single_event {
  type: string
  sql: case when ${event_id} = ${bc_event_id} then 'Single'
  else 'Pass'
  end;;
}
dimension: pass_event_id {
  type: string
  sql: case when ${event_id} != ${bc_event_id} then ${bc_event_id} else NUll
  end;;
}

  dimension: ticket_status {
    type: string
    sql:
    CASE
        WHEN ${Is_Printed} = 'N' THEN 'NOT RENDERED' -- No Barcode has been rendered yet
        ELSE    -- Barcode has been generated (Printed = 'Y')
            CASE
                WHEN ((${tf_id} IS NOT NULL) OR (${tf_id} != 0)) OR (${pid_id} IS NOT NULL) THEN 'RENDERED'  --Ticket printed with TicketFast(tf_id) OR Access Manager(PID).
                --tf_id==0 means (1) ticketfast id is never been generated (2) means it was generated once, but it became invalid(ex.: ticket was returned,or changed ownership)

                ELSE -- Barcode Generated, but not physically printed or rendered
                    CASE
                        WHEN ${Is_Premptive} = 'Y' THEN --Barcode Preemptive(generated ahead of time)
                            CASE
                                WHEN (${Is_Saved_To_Passbook} = 'Y') OR (${Is_Viewed_As_Ret} = 'Y') THEN 'RENDERED' --if viewed as RET or saved to passbook, then the ticket has been rendered surely
                                ELSE
                                    'NOT RENDERED'
                            END
                        ELSE  -- Barcode NOT Preemptive(not generated ahead of time)
                            CASE
                                WHEN (${Is_Saved_To_Passbook} = 'Y') OR (${Is_Viewed_As_Ret} = 'Y') THEN 'RENDERED'
                                ELSE
                                    'UNKNOWN' -- May be barcode was printed by the box office or delivered to the fan via some other method
                            END
                    END
            END
    END;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
